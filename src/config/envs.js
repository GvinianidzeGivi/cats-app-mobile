import { API_URL } from "@env";

const devEnvVars = {
  API_URL: API_URL,
};

const prodEnvVars = {
  API_URL: API_URL,
};

export default __DEV__ ? devEnvVars : prodEnvVars;
