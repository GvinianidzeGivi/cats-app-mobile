import React, {useState, useEffect} from 'react';
import { createDrawerNavigator, DrawerItem } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import HomeScreen from '../../screens/home/home.screen';
import CategoryScreen from '../../screens/categories/category.screen';
import { RootState } from '../../redux/store';
import { useDispatch, useSelector } from 'react-redux';
import { getCategories, getCats } from '../../redux/ducks/catsReducer';


const DrowerNavigator = () => {

 const dispatch = useDispatch();
  
    const { categories, selectedCategoryId, pageLimit } = useSelector(
    (state: RootState) => state.cats
  );
  
   useEffect(() => {
    dispatch(getCategories());
  }, [dispatch]);

  useEffect(() => {
    if (selectedCategoryId) {
      dispatch(getCats());
    }
  }, [dispatch, pageLimit, selectedCategoryId]);

  console.log();
  
  const Drawer = createDrawerNavigator();

  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="Home">
        <Drawer.Screen name="Home" component={HomeScreen}/>
        {
            categories.map(category => <Drawer.Screen key={category.id} name={category.name} component={CategoryScreen}/>)
        }
      </Drawer.Navigator>
    </NavigationContainer>
  );
}

export default DrowerNavigator;