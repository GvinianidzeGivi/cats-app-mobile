import axios from "axios";
import envs from "../config/envs";

let headers = {};

const axiosInstance = axios.create({
  baseURL: envs.API_URL,
  headers,
});

export default axiosInstance;
