import React, {useEffect} from 'react';
import { Button, View, Text} from 'react-native';
import { useSelector } from 'react-redux';
import { Cat } from '../../models';
import { RootState } from '../../redux/store';

const CategoryScreen: React.FC = () => {
  const { selectedCategoryId, cats} = useSelector((state: RootState) => ({
      selectedCategoryId: state.cats.selectedCategoryId,
      cats: state.cats.results
  }));
    
  
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
    
    </View>
  );
}

export default CategoryScreen;