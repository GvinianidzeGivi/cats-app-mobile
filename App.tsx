import React from "react";
import store from "./src/redux/store";
import DrawerNavigator from './src/navigations/drawer/drawer.navigation';
import { Provider } from "react-redux";

const App: React.FC = () => { 


     return (
      <Provider store={store}>
        <DrawerNavigator />
      </Provider>
    ); 

};

export default App;
